import os
import sys
import subprocess


class Conversion(object):
    
    def __init__(self, source_folder, target_folder):
        self.input_options = '-i'
        self.output_options = '-b 32k'
        self.file_dict = {}
        if source_folder[-1] != '/':
            source_folder = source_folder+'/'
        if target_folder[-1] != '/':
            target_folder = target_folder+'/'
        self.source_folder = source_folder
        self.target_folder = os.path.join(target_folder, 'ScriptOutput/')
    
    def file_collection(self):
        
        for dirpath, dirnames, filenames in os.walk(self.source_folder):
            for file in filenames:
                old_file = os.path.join(dirpath, file)
                new_dir_path = dirpath.replace(self.source_folder, self.target_folder)
                new_file = os.path.join(new_dir_path, file)
                self.file_dict[old_file] = new_file
                self.folder_creation(new_dir_path)
        
    def folder_creation(self, new_dir_path):
        
        paths_build = '/'
        for fol in new_dir_path.split('/'):
            paths_build = os.path.join(paths_build, fol)
            if not os.path.exists(paths_build):
                os.mkdir(paths_build)
    
    def separate_files(self):
        
        self.conversion_files = {}
        self.all_other_files = {}
        
        for old_file, new_file in self.file_dict.items():
            file, ext = os.path.splitext(new_file)
            if ext.lower() not in  ('.mp3','.jpg'):
                self.conversion_files[old_file] =  file+'.mp3'
            else:
                self.all_other_files[old_file] =  new_file
        
        flv_count = 0 
        
        for key, value in self.conversion_files.items():
            value,ext = os.path.splitext(value)
            value = value+'.mp3'
            command = 'avconv -i {0}  {1}'.format(key, value)
            #command = 'avconv -i {0} -b 32k {1}'.format(key, value)
            flv_count += 1
            conv = subprocess.call(command, shell=True)
            print('----------------------------------------------------------')
            print('--------------- Total Files -----------{0}----------------'.format(len(self.conversion_files)))
            print('--------------- Completed Files -------{0}----------------'.format(flv_count))
            print(command)
        
        for key, value in self.all_other_files.items():
            path, name = os.path.split(value)
            command = 'cp {0} {1}'.format(key, path+'/')
            print(command)
            conv = subprocess.call(command, shell=True)
           
if __name__=='__main__':
    source_folder = sys.argv[1].strip()
    target_folder = sys.argv[2].strip()
    conv = Conversion(source_folder, target_folder)
    conv.file_collection()
    conv.separate_files()
    
